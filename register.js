const { randomSleep } = require('./utils');

async function register(page, email, password, securityAnswer) {
  const signIn = '.fa-sign-in-alt';
  await page.waitForSelector(signIn);
  await page.click(signIn);
  await page.waitFor(randomSleep());

  const notYetACustomer = '.mat-drawer-content > .ng-star-inserted > div > .mat-card > .primary-link:nth-child(3)';
  await page.waitForSelector(notYetACustomer);
  await page.click(notYetACustomer);
  await page.waitFor(randomSleep());

  const chooseEmailField = '.form-container > .ng-tns-c8-11 > .mat-form-field-wrapper > .mat-form-field-flex > .mat-form-field-infix';
  await page.waitForSelector(chooseEmailField);
  await page.click(chooseEmailField);
  await page.waitFor(randomSleep());

  const formSelector = '.mat-form-field > .mat-form-field-wrapper > .mat-form-field-flex > .mat-form-field-infix';

  const emailInput = formSelector + ' > #mat-input-3';
  await page.type(emailInput, email);
  await page.waitFor(randomSleep());

  const passwordInput = formSelector + ' > #mat-input-4';
  await page.type(passwordInput, password);
  await page.waitFor(randomSleep());

  const passwordRetypeInput = formSelector + ' > #mat-input-5';
  await page.type(passwordRetypeInput, password);
  await page.waitFor(randomSleep());

  const securityQuestion = formSelector + ' > [name="securityQuestion"]';
  await page.click(securityQuestion);
  await page.waitFor(randomSleep());

  const securityQuestionOption = '#mat-option-69';
  page.waitForSelector(securityQuestionOption);
  page.click(securityQuestionOption);
  await page.waitFor(randomSleep());

  const securityAnswerInput = formSelector + ' > #mat-input-6';
  await page.type(securityAnswerInput, securityAnswer);
  await page.waitFor(randomSleep());

  const registerButton = 'button[color="primary"]';
  page.waitForSelector(registerButton);
  await page.click(registerButton);
}

module.exports = {
  register
};
