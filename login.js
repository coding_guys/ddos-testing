const { randomSleep } = require('./utils');

async function login(page, email, password) {
  const emailInput = 'input#email';
  await page.waitForSelector(emailInput);
  await page.type(emailInput, email);
  await page.waitFor(randomSleep());

  const passwordInput = 'input#password';
  await page.waitForSelector(passwordInput);
  await page.type(passwordInput, password);
  await page.waitFor(randomSleep());

  const loginButton = 'button#loginButton';
  await page.waitForSelector(loginButton);
  await page.click(loginButton);
}

module.exports = {
  login
};
