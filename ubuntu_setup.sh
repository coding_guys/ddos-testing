#!/bin/bash

set -e

sudo apt update && sudo apt upgrade
sudo apt install hping3 -y

# YARN
curl -sS https://dl.yarnpkg.com/debian/pubkey.gpg | sudo apt-key add -
echo "deb https://dl.yarnpkg.com/debian/ stable main" | sudo tee /etc/apt/sources.list.d/yarn.list

# NODE
curl -sL https://deb.nodesource.com/setup_6.x | sudo -E bash -

#CHROME
wget -q -O - https://dl-ssl.google.com/linux/linux_signing_key.pub | sudo apt-key add -
echo 'deb [arch=amd64] http://dl.google.com/linux/chrome/deb/ stable main' | sudo tee /etc/apt/sources.list.d/google-chrome.list

# INSTALL
sudo apt-get update
sudo apt-get install -y yarn nodejs google-chrome-stable

# REPO
mkdir util
cd util/

git clone https://bitbucket.org/coding_guys/ddos-testing.git
cd ddos-testing/

yarn
