#!/bin/bash

set -e
sudo wget -q -O - https://archive.kali.org/archive-key.asc  | sudo apt-key add
curl -sS https://dl.yarnpkg.com/debian/pubkey.gpg | sudo apt-key add -
echo "deb https://dl.yarnpkg.com/debian/ stable main" | sudo tee /etc/apt/sources.list.d/yarn.list
sudo apt-get update && sudo apt-get install -y yarn

curl -sL https://deb.nodesource.com/setup_6.x | sudo -E bash -
sudo apt-get install -y nodejs

wget https://dl.google.com/linux/direct/google-chrome-stable_current_amd64.deb
sudo dpkg -i google-chrome-stable_current_amd64.deb

mkdir util
cd util/

git clone https://bitbucket.org/coding_guys/ddos-testing.git
cd ddos-testing/

yarn
