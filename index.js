const puppeteer = require('puppeteer');
const { register } = require('./register');
const { login } = require('./login');
const { reviewProduct } = require('./reviewProduct');
const { buyProducts } = require('./buyProducts');
const { randomEmail, randomPassword } = require('./utils');

const API_URL = 'http://localhost:4000';

(async () => {
  const browser = await puppeteer.launch({ headless: false });
  const page = (await browser.pages())[0];

  await page.setViewport({ width: 1302, height: 798 });
  await page.goto(API_URL);

  const email = randomEmail();
  const password = randomPassword();
  const securityAnswer = 'answer';
  const productReview = 'review';

  await register(page, email, password, securityAnswer);
  await login(page, email, password);
  await reviewProduct(page, 1, productReview);
  await buyProducts(page, [2, 3, 5]);

  await page.waitFor(100000);

  await browser.close();
})();
