const { selectProduct, closeProduct } = require('./selectProduct');
const { randomSleep } = require('./utils');

async function reviewProduct(page, productIndex, review) {
  await selectProduct(page, productIndex);

  const reviewArea = 'mat-form-field > div > div.mat-form-field-flex > div.mat-form-field-infix > textarea';
  await page.waitForSelector(reviewArea);
  await page.type(reviewArea, review);
  await page.waitFor(randomSleep());

  const submitButton = 'button#submitButton';
  await page.waitForSelector(submitButton);
  await page.click(submitButton);

  await closeProduct(page);
}

module.exports = {
  reviewProduct
};
