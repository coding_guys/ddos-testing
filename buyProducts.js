const { randomSleep } = require('./utils');

async function buyProducts(page, productIndices) {
  for (const productIndex of productIndices) {
    const addToBasketButton = `mat-row.mat-row:nth-child(${productIndex + 1}) > mat-cell > button.ng-star-inserted`;
    await page.waitForSelector(addToBasketButton);
    await page.click(addToBasketButton);
    await page.waitFor(randomSleep());
  }

  const itemPlaced = 'div.ng-star-inserted > p.confirmation';
  await page.waitForSelector(itemPlaced);

  const basketButton = 'button[routerlink="/basket"]';
  await page.waitForSelector(basketButton);
  await page.click(basketButton);
  await page.waitFor(randomSleep());

  const checkoutButton = 'button#checkoutButton';
  await page.waitForSelector(checkoutButton);
  await page.click(checkoutButton);
}

module.exports = {
  buyProducts
};
