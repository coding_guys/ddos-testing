const _ = require('lodash/fp');

function randomSleep(secondMultiplier = 2000) {
  return Math.random() * secondMultiplier + 400;
  // return 1;
}

function randomNumber() {
  return Math.floor(Math.random() * 100000);
}

function randomEmail() {
  return `test${randomNumber()}@test.com`;
}

function randomPassword() {
  return `pass${randomNumber()}`;
}

function randomReview() {
  return _.reduce((acc, number) => `${acc} ${randomNumber()}`, '', _.range(1, Math.floor(Math.random() * 30)));
}

function randomSecurityAnswer() {
  return `${randomNumber()}`;
}

module.exports = {
  randomSleep,
  randomEmail,
  randomPassword,
  randomReview,
  randomSecurityAnswer
};
