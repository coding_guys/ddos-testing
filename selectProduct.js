const { randomSleep } = require('./utils');

async function selectProduct(page, productIndex) {
  const productSelector = `mat-row.mat-row:nth-child(${productIndex + 1}) > mat-cell.cdk-column-Image`;
  await page.waitForSelector(productSelector);
  await page.click(productSelector);
}

async function closeProduct(page) {
  const closeButton = 'button.close-dialog';
  await page.waitForSelector(closeButton);
  await page.click(closeButton);
}

module.exports = {
  selectProduct,
  closeProduct
};
