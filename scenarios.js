const puppeteer = require('puppeteer');
const random = require('random');
const sleep = require('await-sleep');

const { register } = require('./register');
const { login } = require('./login');
const { selectProduct, closeProduct } = require('./selectProduct');
const { reviewProduct } = require('./reviewProduct');
const { buyProducts } = require('./buyProducts');
const { randomSleep, randomEmail, randomPassword } = require('./utils');
const _ = require('lodash/fp');

const API_URL = 'http://192.168.0.100:3000';
// const API_URL = 'http://192.168.5.2:3000';
// const API_URL = 'http://localhost:3000';

const concurrentSimulationCount = 5;
const anonymousCount = 8000;
const regularCount = 2000;
const popularProducts = _.range(1, 5);
const unpopularProducts = _.range(5, 10);
const runHealdess = true;

const productSelectRandom = random.normal((mu = 0), (sigma = Math.sqrt(5)));

function productSelectCount() {
  return Math.abs(Math.floor(productSelectRandom()));
}

function randomProduct() {
  if (random.float() > 0.9) return _.sample(unpopularProducts);
  else return _.sample(popularProducts);
}

async function simulateConcurrent(simulationCount) {
  const simulations = _.map(_ => simulate(), _.range(0, simulationCount));
  return Promise.all(simulations);
}

async function simulate() {
  console.log('HELLO WORLD');

  const anonymousUsers = _.map(i => `Anon-${i}`, _.range(0, anonymousCount));
  const regularUsers = _.map(_ => '', Array(regularCount));
  const users = _.shuffle([...anonymousUsers, ...regularUsers]);

  for (const user of users) {
    try {
      console.log(`User: ${user} started.`);
      if (user.startsWith('Anon')) {
        await anonymousScenario();
      } else {
        await regularScenario();
      }

      console.log(`User: ${user} finished.`);
    } catch (e) {
      console.error(e);
    }
    await sleep(randomSleep() + 1000);
  }
}

async function setUp() {
    const browser = await puppeteer.launch({ headless: runHealdess ,args:['--no-sandbox', '--disable-setuid-sandbox']});
  const page = (await browser.pages())[0];

  await page.setViewport({ width: 1302, height: 798 });
    await page.goto(API_URL,{timeout:300000});

  return [browser, page];
}

async function anonymousScenario() {
  const [browser, page] = await setUp();

  const productCount = productSelectCount();
  const productIndices = _.map(_ => randomProduct(), _.range(0, productCount));

  for (productIndex of productIndices) {
    await selectProduct(page, productIndex);
    await page.waitFor(randomSleep() + 1000);
    await closeProduct(page);
  }

  await browser.close();
}

async function regularScenario() {
  const [browser, page] = await setUp();

  const email = randomEmail();
  const password = randomPassword();
  const securityAnswer = 'Doggy';
  const productReview = 'review';

  await register(page, email, password, securityAnswer);
  await login(page, email, password);

  const productIndex = _.first(_.map(_ => randomProduct(), _.range(0, 1)));
  await selectProduct(page, productIndex);
  await page.waitFor(randomSleep());
  await closeProduct(page);
  await page.waitFor(randomSleep());
  await buyProducts(page, [productIndex]);

  await page.waitFor(randomSleep(1000));
  await page.goto(API_URL,{timeout:300000});
  await page.waitFor(randomSleep(600));
  await reviewProduct(page, productIndex, productReview);

  await browser.close();
}

simulateConcurrent(concurrentSimulationCount);
